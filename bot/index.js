const puppeteer = require('puppeteer');
const prompts = require('prompts');
const delay = require('delay');
 
(async () => {
  const response = await prompts([
    {
      type: 'text',
      name: 'login',
      message: 'Instagram login/email'
    },
    {
      type: 'password',
      name: 'password',
      message: 'Instagram password'
    }
]);

  console.log(response);

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://www.instagram.com/accounts/login/');
  await page.waitFor('input[name=username]', { visible: true })
  await delay(300)
  await page.type('input[name=username]', response.login, { delay: 27 })

  await delay(520)
  await page.type('input[name=password]', response.password, { delay: 42 })

  await delay(700)
  const [signup] = await page.$x('//button[contains(.,"Log in")]')
  await Promise.all([
    page.waitForNavigation(),
    signup.click({ delay: 30 }),
    page.screenshot({ path: 'promise.png' })
  ])
 
  await delay(200)
  await page.screenshot({ path: 'before_close.png' });
  await page.close()
  await browser.close();
})();